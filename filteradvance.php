<html>
<body>

<?php
/* variable to check */
$int = 44;
$min = 1;
$max = 100;

if (filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))) === false) {
  echo("Variable value is not within the  range <br>");
} else {
  echo("Variable value is within the  range <br>");
}
$IP = "2001:0db8:85a3:08d3:1319:8a2e:0370:7334";
if (filter_var($IP, FILTER_VALIDATE_IPV6)===false){
  echo("Enter a valid IP address");
  }  else{
  echo($IP . " is not a valid IP address");
  }
?>

</body>
</html>
